import managers.Manager;
import view.ConsoleEvents;
import view.PaintComponents;

public class Main {


    private Main() {
    }

    public static void main(String[] args) throws InterruptedException {
        PaintComponents paintPart = new PaintComponents();
        ConsoleEvents event = new ConsoleEvents();
        Manager manager = new Manager();
        paintPart.paintStart();
        Thread.sleep(3000);
        paintPart.paintStory();
        paintPart.paintLine(4);
        Thread.sleep(5000);
        if (event.startGameMenu().equalsIgnoreCase("yes")) {
            //System.out.println(manager.createArmourShop());
            //System.out.println(manager.createWeaponShop());
            manager.createPlayer(event.createHero(), 100, 0);
        } else {
            System.exit(0);
        }
        manager.upgradePlayer();
    }
}
