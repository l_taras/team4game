package managers;

import exeptions.GameOver;
import exeptions.KillEnemy;
import models.Enemy;
import models.Player;
import tasks.ConsoleInputTest;
import view.Constants;
import view.PaintComponents;

import java.util.Random;
import java.util.Scanner;

public class Battle {
    private static int countBattles = 0;
    private int countAttacks = 0;
    private Player player;
    private Enemy enemy;


    protected Battle(Player player, Enemy enemy) {
        this.player = player;
        this.enemy = enemy;
        countBattles++;
    }

    protected void mainBattle() {
        System.out.println("You go across and see a droid");
        pause(2000);
        System.out.println("Trying to get access... ");
        printPause(". ", 8);
        System.out.println();
        Random random = new Random();
        if (random.nextBoolean()) {
            System.out.println("It looks like we can try to hack a droid");
            if (askYesNo().equals("yes")) {
                printPause("#", 8);
                if (player.getGold() > 0) {
                    player.setGold(player.getGold() - 1);
                    if (ConsoleInputTest.timeTest()) {
                        System.out.println("It has " + enemy.getCurrentWeapon().getName() + "!!!");
                        System.out.println("and " + enemy.getCurrentArmour().getName());
                    }
                } else System.out.println("not enough gold");
            }
        } else {
            System.out.println("Can't connect to the droid. No information");
        }
        System.out.println("Droid see you too");
        try {
            while (battleMenu().equals("attack")) {
                shootPlayer();
                shootEnemy();
            }
            if ((random.nextInt(10) + player.getSpeed()) <= 2) {
                System.out.println(Constants.ANSI_RED + "You are not fast enough");
                pause(2000);
                System.out.println("Enemy are going to shoot!!!" + Constants.ANSI_RESET);
                pause(2000);
                shootEnemy();
                return;
            }
        } catch (GameOver e) {
            System.out.println(Constants.ANSI_RED + "You are dead. Game over");
            PaintComponents paint = new PaintComponents();
            paint.printGameOver();
            System.out.println("You have only " + countBattles + " battles");
            System.exit(0);
        } catch (KillEnemy e) {
            System.out.println(Constants.ANSI_GREEN + "Enemy eliminated. Good job" + Constants.ANSI_RESET);
        }

    }

    private void shootPlayer() throws KillEnemy {
        countAttacks++;
        Random random = new Random();
        printPause("-> ", 12);
        if ((random.nextInt(10) + player.getSpeed()) <= 2) {
            System.out.println(Constants.ANSI_RED + "You missed" + Constants.ANSI_RESET);
            return;
        }
        int damage = player.getCurrentWeapon().getDamage();
        int protection = enemy.getCurrentArmour().getProtection();
        int result = ((damage - protection) * countAttacks / 2) * 2;
        System.out.println("You make " + Constants.ANSI_GREEN + result + Constants.ANSI_RESET + " damage");
        enemy.setHealth(enemy.getHealth() - result);
        if (enemy.getHealth() <= 0) {
            System.out.println("You got " + Constants.ANSI_GREEN + enemy.getGold() + Constants.ANSI_RESET + " gold");
            System.out.println("and " + Constants.ANSI_GREEN + enemy.getSkill() + Constants.ANSI_RESET + " skills");
            player.setGold(player.getGold() + enemy.getGold());
            player.setSkills(player.getSkills() + enemy.getSkill());
            throw new KillEnemy();
        }
        System.out.println("Enemy has " + Constants.ANSI_RED + enemy.getHealth() + Constants.ANSI_RESET + " health");
    }

    private void shootEnemy() throws GameOver {
        Random random = new Random();
        boolean test = false;
        if (random.nextBoolean()) {
            System.out.println(Constants.ANSI_GREEN + "It looks like we can try to avoid enemy's attack!"
                    + Constants.ANSI_RESET);
            printPause("###", 15);
            if (test = ConsoleInputTest.timeTest()) {
                System.out.println(Constants.ANSI_GREEN + "Enemy lost us for a while! You can attack again!"
                        + Constants.ANSI_RESET);
            }
        }
        if (!test) {
            int damage = enemy.getCurrentWeapon().getDamage();
            int protection = player.getCurrentArmour().getProtection();
            int result = (damage - protection) * countAttacks / 2;
            System.out.println("Enemy makes " + Constants.ANSI_RED + result + Constants.ANSI_RESET + " damage");
            player.setHealth(player.getHealth() - result);
            if (player.getHealth() <= 0) {
                throw new GameOver();
            }
            System.out.println("You has " + Constants.ANSI_GREEN + player.getHealth()
                    + Constants.ANSI_RESET + " health");
        }
    }

    private String battleMenu() {
        Scanner input = new Scanner(System.in);
        String answer;
        do {
            System.out.println("What to do? \n ");
            System.out.println("\n Attack |  Run");
            answer = input.nextLine().toLowerCase();
        }
        while (!answer.equals("attack") && !answer.equals("run"));
        return answer;
    }

    private String askYesNo() {
        Scanner input = new Scanner(System.in);
        String answer;
        do {
            System.out.println("Do it? It will cost 1 gold \n ");
            System.out.println("\n YES |  NO");
            answer = input.nextLine().toLowerCase();
        }
        while (!answer.equals("yes") && !answer.equals("no"));
        return answer;
    }

    private void printPause(String sym, int time) {
        for (int i = 0; i < time; i++) {
            System.out.print(sym + " ");
            pause(200);
        }
        System.out.println();
    }

    public static void pause(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}




