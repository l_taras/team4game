package managers;

import models.Enemy;
import models.Player;
import models.items.Armour;
import models.items.Weapon;
import view.ConsoleEvents;
import view.Constants;
import view.PaintComponents;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Manager {
    private Player player;
    private PaintComponents paintComponent = new PaintComponents();
    private ConsoleEvents event = new ConsoleEvents();

    public Manager() {
    }

    /**
     * Create player for game
     *
     * @param name   the name of player
     * @param health the health of player, when <0 player will dye
     * @param speed  the speed of player, helps to avoid damage or run from enemy
     */
    public void createPlayer(String name, int health, int speed) throws InterruptedException {
        player = new Player(name, health, speed);
        paintComponent.paintLine(4);
        System.out.println("Welcome to droid hunters !!!");
        System.out.println(" Your name is " + Constants.ANSI_YELLOW + player.getName() + Constants.ANSI_RESET + "!");
        System.out.println(" You have " + Constants.ANSI_GREEN + player.getHealth() + Constants.ANSI_RESET +
                " points of health and " + Constants.ANSI_GREEN + player.getSpeed() + Constants.ANSI_RESET +
                " level of speed.");
        paintComponent.paintLine(1);
        Thread.sleep(2000);
        System.out.println("Welcome to the Droid Hunter Training Center!\n" +
                "You have completed initial training and received 20 units of experience. " +
                "You can use it to improve survival skills.\n" +
                "You also got the  base hunter set and the first 5 coins of bonus for buying " +
                "ammunition in the store.");
        player.setSkills(20);
        player.setGold(5);
        player.setCurrentArmour(new Armour("Helm", 1, 1));
        player.setCurrentWeapon(new Weapon("Baton", 1, 10));
        paintComponent.paintLine(1);
        Thread.sleep(2000);
    }

    public Player getPlayer() {
        return player;
    }

    public List<Armour> createArmourShop() {
        List<Armour> armourShop = new ArrayList<>();
        armourShop.add(new Armour("Helm", 1, 1));
        armourShop.add(new Armour("Gloves", 5, 3));
        armourShop.add(new Armour("Waistcoat", 10, 5));
        armourShop.add(new Armour("Boots", 20, 7));
        armourShop.add(new Armour("Bulletproof vest", 50, 10));
        return armourShop;
    }

    public List<Weapon> createWeaponShop() {
        List<Weapon> weaponShop = new ArrayList<>();
        weaponShop.add(new Weapon("Baton", 1, 10));
        weaponShop.add(new Weapon("Gun", 5, 15));
        weaponShop.add(new Weapon("Machine gun", 10, 20));
        weaponShop.add(new Weapon("Blaster", 20, 30));
        weaponShop.add(new Weapon("Cannon", 50, 40));
        return weaponShop;
    }

    public void upgradePlayer() {
        paintComponent.paintLine(1);
        String answer = event.upgradeMenu();
        if (answer.equalsIgnoreCase("skill")) {
            paintComponent.paintLine(1);
            answer = event.upgradeSkillsMenu();
            if (answer.equalsIgnoreCase("health")) {
                if (player.getSkills() / 2 <= 0) {
                    System.out.println("Not enough skills");
                    upgradePlayer();
                } else {
                    paintComponent.paintLine(1);
                    int value = event.upgradeHealth(player);
                    player.setSkills(player.getSkills() - value * 2);
                    upgradeHealthPlayer(value);
                    player.setMaxHealthlevel(player.getMaxHealthlevel() + value);
                    System.out.println();
                    player.printEssentialCaracteristics();
                    upgradePlayer();
                }
            } else if (answer.equalsIgnoreCase("speed")) {
                if (player.getSkills() / 20 <= 0) {
                    System.out.println("Not enough skills");
                    upgradePlayer();
                } else {
                    int value = event.upgradeSpeed(player);
                    player.setSkills(player.getSkills() - value * 20);
                    upgradeSpeedPlayer(value);
                    System.out.println();
                    player.printEssentialCaracteristics();
                    upgradePlayer();
                }
            } else if (answer.equalsIgnoreCase("main")) {
                upgradePlayer();
            } else {
                mainGameProcess();
            }
        } else if (answer.equalsIgnoreCase("shop")) {
            shopProcess();

        } else {
            mainGameProcess();
        }
    }

    public void shopProcess() {
        paintComponent.paintLine(1);
        if (player.getGold() < 1) {
            System.out.println("Not enough gold");
            upgradePlayer();
        }
        String ansver = event.shopMenu();
        if (ansver.equalsIgnoreCase("weapon")) {
            shopWeapon();
        } else if (ansver.equalsIgnoreCase("armour")) {
            shopArmour();
        } else if (ansver.equalsIgnoreCase("main")) {
            upgradePlayer();
        } else {
            mainGameProcess();
        }
    }

    private void shopWeapon() {
        paintComponent.paintLine(1);
        System.out.println("Current weapon is " + player.getCurrentWeapon().getName());
        String ansver = event.buyWeapon();
        List<Weapon> weaponsMag = createWeaponShop();

        if (ansver.equalsIgnoreCase("exit")) {
            shopProcess();
        } else if (ansver.equalsIgnoreCase("fault")) {
            System.out.println("No weapon with this name!");
            shopWeapon();
        } else {
            for (Weapon item : weaponsMag) {
                if (item.getName().equalsIgnoreCase(ansver)) {
                    if (item.getCost() <= player.getGold()) {
                        Weapon weapon = player.getCurrentWeapon();
                        player.addWeapon(weapon);
                        player.setCurrentWeapon(item);
                        player.setGold(player.getGold() - item.getCost());
                        System.out.println("You bought " + item.getName() + ". \n " +
                                "Your current weapon is " + player.getCurrentWeapon().getName());
                        System.out.println("You have " + player.getGold() + " gold.");
                        shopWeapon();
                    } else {
                        System.out.println("You don't have enough money.");
                        System.out.println("You have " + player.getGold() + " gold, but need " +
                                item.getCost() + " gold");
                        shopWeapon();
                    }
                }
            }
        }
    }

    private void shopArmour() {
        paintComponent.paintLine(1);
        System.out.println("Current armor is " + player.getCurrentArmour().getName());
        String ansver = event.buyArmour();
        List<Armour> armours = createArmourShop();
        if (ansver.equalsIgnoreCase("exit")) {
            shopProcess();
        } else if (ansver.equalsIgnoreCase("fault")) {
            System.out.println("No armour with this name!");
            shopArmour();
        } else {
            for (Armour item : armours) {
                if (item.getName().equalsIgnoreCase(ansver)) {
                    if (item.getCost() <= player.getGold()) {
                        player.addArmour(player.getCurrentArmour());
                        player.setCurrentArmour(item);
                        player.setGold(player.getGold() - item.getCost());
                        System.out.println("You bought " + item.getName() + ". \n " +
                                "Yor current armour is " + player.getCurrentArmour().getName());
                        System.out.println("You have " + player.getGold() + " gold.");
                        shopArmour();
                    } else {
                        System.out.println("You don't have enough money.");
                        System.out.println("You have " + player.getGold() + " gold, but need " +
                                item.getCost() + " gold");
                        shopArmour();
                    }
                }
            }
        }
    }

    private void mainGameProcess() {
        paintComponent.paintLine(2);
        String answer = event.mainMenu(player);
        if (answer.equalsIgnoreCase("go")) {
            paintComponent.paintLine(2);
            startBattle();
        } else if (answer.equalsIgnoreCase("upgrade")) {
            upgradePlayer();
        } else if (answer.equalsIgnoreCase("sleep")) {
            Random random = new Random();
            if ((random.nextInt(9) <= 1)) {
                System.out.println(Constants.ANSI_RED + "ALL your golds were stolen during a sleep"
                        + Constants.ANSI_RESET);
                player.setGold(0);
            }
            repairProcess();
        } else if (answer.equalsIgnoreCase("inventory")) {
            inventoryProcess();
        } else {
            System.out.println("Exit game");
            System.exit(0);
        }
    }

    private void inventoryProcess() {
        paintComponent.paintLine(1);
        player.printAllCaracteristics();
        paintComponent.paintLine(1);
        String answer = event.changeArmorConsole(player);
        if (answer.equalsIgnoreCase("exit")) {
            mainGameProcess();
        } else if (!player.getWeapons().isEmpty() || !player.getArmour().isEmpty()) {
            for (Weapon j : player.getWeapons()) {
                if (answer.equalsIgnoreCase(j.getName())) {
                    player.addWeapon(player.getCurrentWeapon());
                    player.setCurrentWeapon(j);
                    System.out.println("Current weapon is" + player.getCurrentWeapon().getName());
                    inventoryProcess();
                }
            }
            for (Armour i : player.getArmour()) {
                if (answer.equalsIgnoreCase(i.getName())) {
                    player.addArmour(player.getCurrentArmour());
                    player.setCurrentArmour(i);
                    System.out.println("Current armour is " + player.getCurrentArmour().getName());
                    inventoryProcess();
                }
            }
        } else {
            System.out.println("You don't have item with this name.");
            inventoryProcess();
        }
    }

    private void repairProcess() {
        paintComponent.paintLine(1);
        player.setHealth(player.getMaxHealthlevel());
        System.out.println(" You had a rest and repaired your health! \n" +
                "Your current health is " + player.getHealth());
        paintComponent.paintLine(1);
        mainGameProcess();
    }

    private void upgradeHealthPlayer(int health) {
        player.setHealth(player.getHealth() + health);
    }

    private void upgradeSpeedPlayer(int speedLevel) {
        player.setSpeed(player.getSpeed() + speedLevel);

    }

    /**
     * Battle process
     */
    public void startBattle() {
        paintComponent.paintLine(2);
        Battle battle = new Battle(this.player, getEnemy());
        battle.mainBattle();
        mainGameProcess();
    }

    /**
     * Method for creating an enemy
     */
    public Enemy getEnemy() {
        Random random = new Random();
        int temp = random.nextInt(3) * player.getSpeed();
        Enemy enemy = new Enemy("enemy", 100,
                6 + random.nextInt(15) + player.getSpeed(),
                1 + temp);
        enemy.setCurrentArmour(createArmourShop().get(random.nextInt(createArmourShop().size())));
        enemy.setCurrentWeapon(createWeaponShop().get(random.nextInt(createWeaponShop().size())));
        return enemy;
    }

}