package models;

public abstract class Droid {
    private String name;
    private int health;
    private int skill;

    public Droid(String name, int health, int skill) {
        this.name = name;
        this.health = health;
        this.skill = skill;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", skill=" + skill +
                '}';
    }
}
