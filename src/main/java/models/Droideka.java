package models;

public class Droideka extends Droid {
private int cannon;

    public int getCannon() {
        return cannon;
    }

    public void setCannon(int cannon) {
        this.cannon = cannon;
    }

    public Droideka(String name, int health, int skill, int cannon) {
        super(name, health, skill);
        this.cannon = cannon;
    }

    @Override
    public String toString() {
        return "Droideka{" +
                "cannon=" + cannon +
                '}';
    }
}
