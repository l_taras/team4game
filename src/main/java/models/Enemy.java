package models;

import models.items.Armour;
import models.items.Weapon;

public class Enemy  extends Droid{
    /**
     * Gold for player after win
     */
    private int gold;
    private Weapon currentWeapon;
    private Armour currentArmour;

    public Enemy(String name, int health, int skill, int gold) {
        super(name, health, skill);
        this.gold = gold;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public Weapon getCurrentWeapon() {
        return currentWeapon;
    }

    public void setCurrentWeapon(Weapon currentWeapon) {
        this.currentWeapon = currentWeapon;
    }

    public Armour getCurrentArmour() {
        return currentArmour;
    }

    public void setCurrentArmour(Armour currentArmour) {
        this.currentArmour = currentArmour;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Enemy{");
        sb.append("gold=").append(gold);
        sb.append(", currentWeapon=").append(currentWeapon);
        sb.append(", currentArmour=").append(currentArmour);
        sb.append('}');
        return sb.toString();
    }
}
