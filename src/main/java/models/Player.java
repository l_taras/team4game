package models;

import models.items.Armour;
import models.items.Weapon;

import java.util.ArrayList;

/**
 * This class describe main hero of the game
 *
 * @author team4
 * @version 1.1 from 27.09.2018
 */
public class Player {
    private String name;
    private int health;
    private int maxHealthlevel;
    private int speed;
    private int gold;
    private int skills;
    private Weapon currentWeapon;
    private Armour currentArmour;
    private ArrayList<Weapon> weapons = new ArrayList<>();
    private ArrayList<Armour> armour = new ArrayList<>();

    public Player() {
    }

    /**
     * constructor for player
     *
     * @param name   the name of player
     * @param health the health of player, when <0 player will dye
     * @param speed  the speed of player, helps to avoid damage or run from enemy
     */
    public Player(String name, int health, int speed) {
        this.name = name;
        this.health = health;
        this.speed = speed;
        this.maxHealthlevel = health;
    }

    public int getMaxHealthlevel() {
        return maxHealthlevel;
    }

    public void setMaxHealthlevel(int maxHealthlevel) {
        this.maxHealthlevel = maxHealthlevel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeapons(ArrayList<Weapon> weapons) {
        this.weapons = weapons;
    }

    public void setArmour(ArrayList<Armour> armour) {
        this.armour = armour;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getSpeed() {
        return speed;
    }

    public int getGold() {
        return gold;
    }

    public int getSkills() {
        return skills;
    }

    public Weapon getCurrentWeapon() {
        return currentWeapon;
    }

    public Armour getCurrentArmour() {
        return currentArmour;
    }

    public ArrayList<Weapon> getWeapons() {
        return weapons;
    }

    public ArrayList<Armour> getArmour() {
        return armour;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void setSkills(int skills) {
        this.skills = skills;
    }

    public void setCurrentWeapon(Weapon currentWeapon) {
        this.currentWeapon = currentWeapon;
    }

    public void setCurrentArmour(Armour currentArmour) {
        this.currentArmour = currentArmour;
    }

    public void addWeapon(Weapon weapon) {
        weapons.add(weapon);
    }

    public void addArmour(Armour item) {
        armour.add(item);
    }


    public void printCaracteristics() {
        System.out.println();
    }

    public void printEssentialCaracteristics() {
        System.out.println(getName() + "!");
        System.out.println("You have " + getHealth() + " of health, " + getSpeed() +
                " level of speed and can improve it using " + getSkills() + " of skills!");
    }

    /**
     * method print all caracteristics, can be used for test
     */
    public void printAllCaracteristics() {
        System.out.println("Your name is " + getName() + ".");
        System.out.println("You have " + getHealth() + " points of health and " +
                getSpeed() + " level of speed!");
        System.out.println("You have " + getSkills() + " points of skills for improving characteristics and " +
                getGold() + " gold to buy weapon and armour!");
        System.out.println("Your current weapon is " + getCurrentWeapon().getName() + " with damage " +
                getCurrentWeapon().getDamage());
        System.out.println("Your current armour is " + getCurrentArmour().getName() + " with protection " +
                getCurrentArmour().getProtection());
        if (weapons.isEmpty() && armour.isEmpty()) {
            System.out.println("You don't have reserve weapon or armour!");
        } else {
            System.out.println("Also you have ");
            if (!weapons.isEmpty()) {
                for (Weapon i : weapons) {
                    System.out.println(i.getName() + " with damage " + i.getDamage());
                }
            }
            if (!armour.isEmpty()) {
                for (Armour j : armour) {
                    System.out.println(j.getName() + " with protection " + j.getProtection());
                }
            }

        }


    }


    @Override
    public String toString() {
        return "Player{" + super.toString() +
                "gold=" + gold +
                ", velocity=" + speed +
                '}';
    }
}
