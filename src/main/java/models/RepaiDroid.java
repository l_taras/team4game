package models;

public class RepaiDroid extends Droid {
    private int repairSkill;

    public RepaiDroid(String name, int health, int skill, int repairSkill) {
        super(name, health, skill);
        this.repairSkill = repairSkill;
    }

    public int getRepairSkill() {
        return repairSkill;
    }

    public void setRepairSkill(int repairSkill) {
        this.repairSkill = repairSkill;
    }

    @Override
    public String toString() {
        return "RepaiDroid{" +
                "repairSkill=" + repairSkill +
                '}';
    }
}
