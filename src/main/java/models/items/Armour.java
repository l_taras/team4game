package models.items;

/**
 * Model that describe protection of Armour
 *
 * @author team4
 * @version 1.1 from 27.09.2018
 */
public class Armour extends Item {
    private int protection;

    public Armour() {
    }

    /**
     * Constructor for a new Armour.
     *
     * @param name       the name of Armour
     * @param cost       the cost an amount that has to be paid in order to get Armour
     * @param protection the protection from damage
     */
    public Armour(String name, int cost, int protection) {
        super(name, cost);
        this.protection = protection;
    }

    /**
     * Gets protection.
     *
     * @return the protection from damage
     */
    public int getProtection() {
        return protection;
    }

    /**
     * Sets protection.
     *
     * @param protection the protection from damage
     */
    public void setProtection(int protection) {
        this.protection = protection;
    }

    @Override
    public String toString() {
        return "Armour{" + super.toString() +
                "protection=" + protection +
                '}';
    }

}
