package models.items;

/**
 * describe Item Model for Players in the game
 *
 * @author team4
 * @version 1.1 from 27.09.2018
 */
public class Item {
    private String name;
    private int cost;


    public Item() {
    }

    /**
     * Constructor for Item
     *
     * @param name the name of Item
     * @param cost an amount that has to be paid in order to get something (Item).
     * @see Item#Item()
     */
    public Item(String name, int cost) {
        this.name = name;
        this.cost = cost;
    }


    /**
     * Gets name.
     *
     * @return name name of Item
     */
    public String getName() {
        return name;
    }


    /**
     * Sets name.
     *
     * @param name the name for Item
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets cost.
     *
     * @return the cost for paid
     */
    public int getCost() {
        return cost;
    }

    /**
     * Sets cost.
     *
     * @param cost the cost for paid
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                '}';
    }


}
