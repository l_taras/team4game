package models.items;

/**
 * base Model for Weapon
 *
 * @author team4
 * @version 1.1 from 27.09.2018
 */
public class Weapon extends Item {

    private int damage;

    public Weapon() {
    }

    /**
     * Constructor for a new Weapon.
     *
     * @param name   the name of Weapon
     * @param cost   the cost an amount that has to be paid in order to get Weapon
     * @param damage the damage that provides to enemy
     */
    public Weapon(String name, int cost, int damage) {
        super(name, cost);
        this.damage = damage;
    }

    /**
     * Gets damage.
     *
     * @return the Weapon damage
     */
    public int getDamage() {
        return damage;
    }

    /**
     * Sets damage.
     *
     * @param damage the damage of Weapon
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        return "Weapon{" + super.toString() +
                "damage=" + damage +
                '}';
    }
}
