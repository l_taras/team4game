package tasks;

import view.Constants;

import java.security.SecureRandom;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;


public class ConsoleInputTest {
    public static String getRandomString(int len) {
        String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


    public static boolean timeTest() {
        int randomNum = ThreadLocalRandom.current().nextInt(5, 11);
        int randomNum2 = ThreadLocalRandom.current().nextInt(-3, 2);
        int correlation = randomNum + randomNum2;
        String test = getRandomString(randomNum);
        System.out.println("Input " + Constants.ANSI_BLUE + test.toUpperCase()
                + Constants.ANSI_RESET + " as fast as you can");
        System.out.println("You have got " + Constants.ANSI_YELLOW + correlation
                + Constants.ANSI_RESET + " seconds!");
        ConsoleInput con = new ConsoleInput(1, correlation, TimeUnit.SECONDS);
        try {
            String input = con.readLine();
            if (test.toLowerCase().equals(input.toLowerCase())) {
                System.out.println("Done. Your input was in time ");
                return true;
            }
        } catch (Exception e) {
            System.out.println("Oh no!");
            return false;
        }
        return false;
    }
}


