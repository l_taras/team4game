package view;

import managers.Manager;
import models.Player;

import java.util.InputMismatchException;

import models.items.Armour;
import models.items.Weapon;

import java.util.List;
import java.util.Scanner;

public class ConsoleEvents {
    public String startGameMenu() {
        Scanner input = new Scanner(System.in);
        String answer;
        do {
            System.out.println("Do you want to start game? (Type answer) \n Yes  |   No () ");
            answer = input.nextLine();
        }
        while (!answer.equalsIgnoreCase("yes") && !answer.equalsIgnoreCase("no"));
        return answer;
    }

    public String createHero() {
        Scanner input = new Scanner(System.in);
        String answer;
        System.out.println("Enter your name: ");
        String name = input.nextLine();
        do {
            System.out.println("Are you sure? (Type answer) \n Yes  |   No () ");
            answer = input.nextLine();
        }
        while (!answer.equalsIgnoreCase("yes") && !answer.equalsIgnoreCase("no"));
        if (answer.equalsIgnoreCase("no")) {
            createHero();
        }
        return name;
    }

    /**
     * method to display general upgrade menu
     *
     * @return ansver of player
     */
    public String upgradeMenu() {
        Scanner input = new Scanner(System.in);
        String answer;
        do {
            System.out.println("Do you want to improve skills or buy ammunition or continue game?");
            System.out.println("Choose on of the follows. (Type answer) \n Skill  |   Shop  |  Exit");
            answer = input.nextLine();
        }
        while (!answer.equalsIgnoreCase("skill") && !answer.equalsIgnoreCase("shop")
                && !answer.equalsIgnoreCase("exit"));
        return answer;
    }

    /**
     * method to display skills upgrade menu
     *
     * @return ansver of player
     */
    public String upgradeSkillsMenu() {
        Scanner input = new Scanner(System.in);
        String ansver;
        do {
            System.out.println("Welcome to the Genetics Center. Here you can buy 10 health points for " +
                    "20 skills points units or 1 level of speed for 20 skills points. \n ");
            System.out.println("What skills do you want to improve? ");
            System.out.println("Choose on of the follows. (Type answer) \n health  |  speed  |  main  |  exit");
            ansver = input.nextLine();
        }
        while (!ansver.equalsIgnoreCase("health") && !ansver.equalsIgnoreCase("speed")
                && !ansver.equalsIgnoreCase("exit") && !ansver.equalsIgnoreCase("main"));
        return ansver;
    }

    public int upgradeHealth(Player player) {
        System.out.println("10 health points cost 20 skills points units ");
        System.out.println("You have " + player.getHealth() + " health and " +
                player.getSkills() + " skills.");
        System.out.println("How much health points do you want to buy? Or put '0' to exit");
        Scanner input = new Scanner(System.in);
        int ansver = 0;
        do {
            ansver = getAnswer(input, ansver);
            if (ansver < 0 || ansver > player.getSkills() / 2) {
                System.out.println("You don't have such amount of skills. Enter again! Or put '0' to exit");
            }
        }
        while (ansver < 0 || ansver > player.getSkills() / 2);
        player.printEssentialCaracteristics();
        return ansver;
    }

    private int getAnswer(Scanner input, int answer) {
        try {
            answer = input.nextInt();
            System.out.println(
                    "The number entered is " + answer);
        } catch (InputMismatchException ex) {
            System.out.println("Try again. (" +
                    "Incorrect input: an integer is required)");
            input.nextLine();
        }
        return answer;
    }

    public int upgradeSpeed(Player player) {
        System.out.println("1 speed level cost 20 skills points units ");
        System.out.println("You have " + player.getSpeed() + " level of speed and " +
                player.getSkills() + " skills.");
        System.out.println("How much speed levels do you want to buy? Or put '0' to exit.");
        Scanner input = new Scanner(System.in);
        int ansver = 0;
        do {
            ansver = getAnswer(input, ansver);
            if (ansver < 0 || ansver > player.getSkills() / 20) {
                System.out.println("You don't have such amount of skills. Enter again! Or put '0' to exit");
            }
        }
        while (ansver < 0 || ansver > player.getSkills() / 20);
        player.printEssentialCaracteristics();
        return ansver;
    }

    public String shopMenu() {
        Scanner input = new Scanner(System.in);
        String ansver;
        do {
            System.out.println("Welcome to the Arsenal. Here you can buy weapon and armour. \n ");
            System.out.println("What do you want to buy? ");
            System.out.println("Choose on of the follows. (Type answer) \n weapon  |  armour  |  main  |  exit");
            ansver = input.nextLine();
        }
        while (!ansver.equalsIgnoreCase("weapon") && !ansver.equalsIgnoreCase("armour")
                && !ansver.equalsIgnoreCase("exit") && !ansver.equalsIgnoreCase("main"));
        return ansver;
    }

    public String buyWeapon() {
        System.out.println("Here you can buy weapon. \n We can propose you:");
        Manager man = new Manager();
        List<Weapon> weaponShop = man.createWeaponShop();
        for (Weapon i : weaponShop) {
            System.out.println(i.getName() + " with damage " + i.getDamage() + " cost " + i.getCost());
        }
        System.out.println("Type the name of weapon you want to by or exit for back to the upgrade menu");
        Scanner input = new Scanner(System.in);
        String ansver = input.nextLine();
        if (ansver.equalsIgnoreCase("exit")) {
            return ansver;
        }
        for (Weapon i : weaponShop) {
            if (ansver.equalsIgnoreCase(i.getName())) {
                //System.out.println(i.getName());
                return ansver;

            }
        }
        return "fault";
    }

    public String buyArmour() {
        System.out.println("Here you can buy armour. \n We can propose you:");
        Manager manager = new Manager();
        List<Armour> armourShop = manager.createArmourShop();
        for (Armour i : armourShop) {
            System.out.println(i.getName() + " with protection " + i.getProtection() + " cost " + i.getCost());
        }
        System.out.println("Type the name of armour you want to by or exit for back to the upgrade menu");
        Scanner input = new Scanner(System.in);
        String ansver = input.nextLine();
        if (ansver.equalsIgnoreCase("exit")) {
            return ansver;
        }
        for (Armour i : armourShop) {
            if (ansver.equalsIgnoreCase(i.getName())) {
                return ansver;
            }
        }
        return "fault";
    }

    public String changeArmorConsole(Player player) {
        System.out.println("Type item name if you want put it in current position or 'exit' for back to main menu");
        Scanner input = new Scanner(System.in);
        String ansver = input.nextLine();
        if (ansver.equalsIgnoreCase("exit")) {
            return ansver;
        }
        for (Weapon j : player.getWeapons()) {
            if (ansver.equalsIgnoreCase(j.getName())) {
                return ansver;
            }
        }
        for (Armour i : player.getArmour()) {
            if (ansver.equalsIgnoreCase(i.getName())) {
                return ansver;
            }
        }
        return "fault";
    }

    public String mainMenu(Player player) {
        Scanner input = new Scanner(System.in);
        String answer;

        do {
            System.out.println("Now you are ready for adventure!!!");
            System.out.println("You have " + player.getHealth() + " health and " + player.getGold() + " gold");
            System.out.println("Choose on of the follows. (Type answer) " +
                    "\n GO hunt  |   Upgrade  |   Sleep | Inventory  |  Exit");
            answer = input.nextLine().toLowerCase();
        }
        while (!answer.equals("go") && !answer.equals("upgrade")
                && !answer.equals("sleep") && !answer.equals("inventory") && !answer.equals("exit"));
        return answer;
    }


}
