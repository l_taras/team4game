package view;

/**
 * The type Constants.
 */
public class Constants {

    /**
     * The constant ANSI_RESET for reset color
     */
    public static final String ANSI_RESET = "\u001B[0m";
    /**
     * The constant ANSI_BLACK for set BLACK color.
     */
    public static final String ANSI_BLACK = "\u001B[30m";
    /**
     * The constant ANSI_RED for set RED color.
     */
    public static final String ANSI_RED = "\u001B[31m";
    /**
     * The constant ANSI_GREEN for set GREEN color.
     */
    public static final String ANSI_GREEN = "\u001B[32m";
    /**
     * The constant ANSI_YELLOW for set YELLOW color.
     */
    public static final String ANSI_YELLOW = "\u001B[33m";
    /**
     * The constant ANSI_BLUE for set BLUE color.
     */
    public static final String ANSI_BLUE = "\u001B[34m";
    /**
     * The constant ANSI_PURPLE for set PURPLE color.
     */
    public static final String ANSI_PURPLE = "\u001B[35m";
    /**
     * The constant ANSI_CYAN for set CYAN color.
     */
    public static final String ANSI_CYAN = "\u001B[36m";
    /**
     * The constant ANSI_WHITE for set WHITE color.
     */
    public static final String ANSI_WHITE = "\u001B[37m";

    /**
     * The constant MAX_LINE_LENGTH for set maximum symbols in console text line.
     */
    public static final int MAX_LINE_LENGTH = 112;

}
