package view;

/**
 * class for paint elements
 *
 * @author team4
 * @version 1.1 from 27.09. 2018
 */
public class PaintComponents {

    /**
     * Paint start picture in text mode.
     */
    public void paintStart() {
        paintLine(4);
        System.out.println();
        System.out.print("                            ");
        System.out.println("##########     #######     ########    ###   ##########     #######    ");
        System.out.print("                            ");
        System.out.println("###      ##    ##    ##   ##      ##         ###      ##   ##     ##   ");
        System.out.print("                            ");
        System.out.println("###       ##   ##    ##   ##      ##   ###   ###       ##  ##          ");
        System.out.print("                            ");
        System.out.println("###       ##   ######     ##      ##   ###   ###       ##   ######     ");
        System.out.print("                            ");
        System.out.println("###       ##   ##  ##     ##      ##   ###   ###       ##         ##   ");
        System.out.print("                            ");
        System.out.println("###      ##    ##   ##    ##      ##   ###   ###      ##   ##     ##   ");
        System.out.print("                            ");
        System.out.println("##########     ##    ##    ########    ###   ##########     #######    ");
        System.out.println();
        System.out.println();
        paintLine(4);
    }

    public void printGameOver() {
        paintLine(4);
        System.out.println();
        System.out.println();
        System.out.println("    ########          #       ##      ##  ########      #######   ###       ###  #######  ######  ");
        System.out.println("   ###     ##       ####      ###    ###  ##           ##     ##   ##       ##   ##       ##   ## ");
        System.out.println("   ###             ##  ##     ####  ####  ##           ##     ##    ##     ##    ##       ##   ## ");
        System.out.println("   ###            ##    ##    ## #### ##  ########     ##     ##     ##   ##     #######  ######  ");
        System.out.println("   ###   ####    ##########   ##  ##  ##  ##           ##     ##      ## ##      ##       ## ##   ");
        System.out.println("   ###     ##   ##       ##   ##      ##  ##           ##     ##       ###       ##       ##  ##  ");
        System.out.println("    ########   ###        ##  ##      ##  ########      #######         #        #######  ##  ##  ");
        System.out.println();
        System.out.println();
        paintLine(4);
    }


    /**
     * Paint line in console.
     *
     * @param n the number of lines for print "#" in console
     */
    public void paintLine(int n) {
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < Constants.MAX_LINE_LENGTH; i++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }

    public void paintStory (){
        System.out.println("The game takes place in space, in the distant future, at the " +
                "beginning of the XXXI century. The galaxy is inhabited by 5 intelligent " +
                "civilizations. All races are included in a single Galactic Commonwealth, " +
                "in which a single time calculation and a single currency is a galactic credit. " +
                "" +  "Also there is a race of aggressors - droids. In connection with the " +
                "invasions of droids, conflicts and wars between races are prohibited.\n" +
                "\n" + "For the first time the expedition explored the second " +
                "branch of the Galaxy. All attempts to contact them ended in failure, " +
                "as the droids opened fire. And although they seemed not to know about the " +
                "inhabited sleeve of the Galaxy (where the action of the game takes place), they " +
                "somehow found a way there.\n" + "The plot is based on the invasion of the " +
                "galaxy by a new, unknown before this form of life - droids. To combat them, the " +
                "organization of the Hunters was founded. The player will act as one of the space " +
                "hunters, who can engage in a variety of activities, but ultimately pursue the goal " +
                "of saving the galaxy from invading the droids.\n");
    }
}
