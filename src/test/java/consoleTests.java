import managers.Manager;
import models.Player;
import models.items.Armour;
import models.items.Weapon;
import view.ConsoleEvents;
import view.PaintComponents;

public class consoleTests {

    public static void main(String[] args) throws InterruptedException {
        PaintComponents paint = new PaintComponents();
        ConsoleEvents console = new ConsoleEvents();
        //System.out.println(console.upgradeMenu());
        Manager man = new Manager();
        man.createPlayer("Harry", 100, 0);
        Player player = new Player();
        player.setSkills(100);
        player.setGold(10);
        player.setCurrentArmour(new Armour("Helm", 1, 5));
        player.setCurrentWeapon(new Weapon("Baton", 1, 10));
        //man.upgradePlayer();
        man.shopProcess();

    }
}
